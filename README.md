# Icinga Module for Graphite

[![PHP Support](https://img.shields.io/badge/php-%3E%3D%207.2-777BB4?logo=PHP)](https://php.net/)
![Build Status](https://github.com/icinga/icingaweb2-module-graphite/workflows/PHP%20Tests/badge.svg?branch=master)
[![Github Tag](https://img.shields.io/github/tag/Icinga/icingaweb2-module-graphite.svg)](https://github.com/Icinga/icingaweb2-module-graphite)

![Icinga Logo](https://icinga.com/wp-content/uploads/2014/06/icinga_logo.png)

This module integrates an existing [Graphite](https://graphite.readthedocs.io/en/latest/)
installation in your [Icinga Web 2](https://icinga.com/products/infrastructure-monitoring/)
frontend.

![Service List](doc/img/service-list.png)
![Detail View](doc/img/service-detail-view.png)

It provides a new menu section with two general overviews for hosts and
services as well as an extension to the host and service detail view of
the monitoring module.

## Documentation

* [Installation](doc/02-Installation.md)
* [Configuration](doc/03-Configuration.md)
* [Templates](doc/04-Templates.md)
* [Demonstration](doc/06-Demonstration.md)
